#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:39e0e4538a630ea2b1ad5a468843c0a0766336ea; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:08e9fd071703574b556da55f2b689277911aed5f EMMC:/dev/block/platform/bootdevice/by-name/recovery 39e0e4538a630ea2b1ad5a468843c0a0766336ea 67108864 08e9fd071703574b556da55f2b689277911aed5f:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
